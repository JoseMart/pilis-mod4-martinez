# Trabajo Final 💻 

Módulo 4: Desarrollo backend con Node.

_Desarrollar un sistema backend de reserva de tickets_

_El sistema consta de Usuarios, Eventos y Reservas, solo se tendra el acceso al edpoint de Eventos, para poder acceder a todos los endpoints, se devera logear con un usuario. Despues de logearse, el usuario podra realizar una Reserva de un Evento, tambien tendra acceso a todos los edpoints._

## Instalación 📋

* _Requerido instalar Doker en el Sistema Operativo https://docs.docker.com/get-docker_

Instalar la base de datos mysql con docker-compose,  ejecutar el comando desde la
ubicación del archivo: docker-compose.yml

```
docker-compose up
```

Instalar las dependencias del proyecto.

```
npm install
```

Correr el proyecto.

```
npm run dev
```

_Despues de iniciar el proyecto. Ver la Documentacion en: http://localhost:3000/docs_

## Construido con 🛠️

* [NodeJs](https://nodejs.org/en)
* [Doker](https://docs.docker.com/)
* [ExpressJs](https://expressjs.com/es/)
* [TypeORM](https://typeorm.io/)
* [JWT](https://jwt.io/)
* [Swagger](https://swagger.io/)

***

Autor: José Ariel Martinez 😊
